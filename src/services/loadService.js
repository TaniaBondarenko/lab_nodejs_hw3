const { Load } = require('../models/loadModel');
const { Truck } = require('../models/truckModel');

const addLoadToShipper = async (loadPayload, userId) => {
    const load = new Load({ ...loadPayload, userId });
    load.status = 'POSTED';
    await load.save();
};

const getLoadsForDriver = async(userId,status) => {
    const loads = await Load.find({ assigned_to:userId, status });
    if (!loads) {
        return "No active loads"
    }
    return loads;
}

const getLoadsForShipper = async(userId) => {
    const loads = await Load.find({ userId: userId });
    console.log(loads)
    if (!loads) {
        return "No active loads"
    }
    return loads;
}

const getActiveLoad = async (userId) => {
        const load = await Load.findOne({
        assigned_to: userId,
        status:'ASSIGNED'
    });
    if (!load) {
        return "No active loads"
    }
}

const getLoadById = async (loadId, userId) => {
    const load = await Load.findOne({ _id: loadId, userId });
    return load;
}

const updateLoadData = async (loadId, userId,data) => {
    const load = await Load.findOneAndUpdate({ _id: loadId,userId }, { $set: data }, {new:true});
return load;
}

const setNextState = async (userId) => {
    const loadStatus = [null, "NEW", "POSTED", "ASSIGNED", "SHIPPED"];
    const loadState = [null, "En route to Pick Up", "Arrived to Pick Up", "En route to delivery", "Arrived to delivery"];
    let load = await Load.findOne({ userId: userId });
    if (load === null) {
        console.log('no loads found')
    }
    let currentIndex;
    for (i = 0; i < loadStatus.length; i++) {
        if (loadStatus[i] === load.status) {
            currentIndex = i;
        }
    }
    if (currentIndex === loadStatus.lenght - 1) {
        load= await Load.findOneAndUpdate({ userId: userId }, { $set: { state: null, status: null } }, { new: true })
    } else {
        load= await Load.findOneAndUpdate({ userId: userId }, { $set: { state: loadState[currentIndex + 1], status: loadStatus[currentIndex + 1] } }, { new: true })
    }
    return load.state;
}
    
const findDriver = async (loadId) => {
    const load = await Load.findOne({ _id: loadId });
    const payload = load.payload;
    const dimensions = load.dimensions;
    const types = {
        "SPRINTER":{ width: 300, length: 250, height: 170, payload:1700 }, 
        "SMALL STRAIGHT":{ width: 500, length: 250, height: 170, payload:2500 }, 
        "LARGE STRAIGHT":{ width: 700, length: 350, height: 200, payload:4000 }, 
    }
    let truckType;
    if (dimensions.width < types["SPRINTER"].width
        && dimensions.length < types["SPRINTER"].length
        && dimensions.height < types["SPRINTER"].height
        && payload < types["SPRINTER"].payload) {
        truckType = "SPRINTER";
} else if (dimensions.width < types["SMALL STRAIGHT"].width
        && dimensions.length < types["SMALL STRAIGHT"].length
        && dimensions.height < types["SMALL STRAIGHT"].height
        && payload < types["SMALL STRAIGHT"].payload) {
        truckType = "SMALL STRAIGHT";
} else if (dimensions.width < types["LARGE STRAIGHT"].width
        && dimensions.length < types["LARGE STRAIGHT"].length
        && dimensions.height < types["LARGE STRAIGHT"].height
        && payload < types["LARGE STRAIGHT"].payload) {
        truckType = "LARGE STRAIGHT";
}
    const truck = await Truck.findOneAndUpdate({ type: truckType, assigned_to: { $ne: null } }, { status: "OL" }, { new: true });
    await Load.findOneAndUpdate({ _id: loadId }, { $set: { state: "En route to Pick Up", status: "ASSIGNED" } }, { new: true });
    if (!truck) {
        await Load.findOneAndUpdate({ _id: loadId }, { $set: { state: null, status: "NEW" } }, { new: true });
        return "No trucks found";
    }
    return truck;
}

module.exports = {
    addLoadToShipper,
    getLoadsForDriver,
    getLoadsForShipper,
    getActiveLoad,
    getLoadById,
    updateLoadData,
    setNextState,
    findDriver
}