const { Truck } = require('../models/truckModel');
const { InvalidRequestError } = require('../utils/errors');

const addTruckToDriver = async (truckPayload, userId,created_by) => {
    created_by = userId;
        const truck = new Truck({ ...truckPayload, userId, created_by });
        await truck.save();
}

const getTrucksByDriverId = async (userId) => {
    const trucks = await Truck.find({ userId });
    return trucks;
}

const getTruckById = async (truckId, userId) => {
    const truck = await Truck.findOne({ _id: truckId, userId });
    return truck;
}

const updateTruck = async (truckId, userId,type) => {
    const truck = await Truck.findOneAndUpdate({ _id: truckId }, { type: type }, {new:true});
return truck;
}

const assignTruckToDriver = async (truckId, userId) => {
    const truck = await Truck.findOneAndUpdate(
        { _id: truckId }, { $set: { assigned_to: userId } }, { new: true });
    return truck;
}

module.exports = {
    addTruckToDriver,
    getTrucksByDriverId, getTruckById,
    updateTruck,
    assignTruckToDriver
}