const mongoose = require('mongoose');

const Load = mongoose.model('Loads',
  {
      userId: {
            type: mongoose.Schema.Types.ObjectId,

        },
        created_by: {
        type: mongoose.Schema.Types.ObjectId
          
        },
        assigned_to: {
      type: mongoose.Schema.Types.ObjectId  
    },
    status: {
        type: String,
      enum: [null,"NEW", "POSTED", "ASSIGNED", "SHIPPED"],
      default: null
    },
    state: {
            type: String,
            enum: [null,"En route to Pick Up", "Arrived to Pick Up", "En route to delivery", "Arrived to delivery"],
    default:null    
    },
    name: String,
    payload: {
      type: Number,
      required: true
    },
    pickup_address:{
      type: String,
       required:true
        },
    delivery_address: {
      type: String,
       required:true
        },
    dimensions: {
           width: {type: Number },
          length: { type: Number },
          height: { type: Number}
    },
        logs: [
    {
      message: {
        type: String,
      },
      time: {
        type: Date,
        default: Date.now(),
            },
    },
        ],
    created_date: {
        type: Date,
        default: Date.now()
    }
});    
    
module.exports = {
        Load
    }