const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const { asyncWrapper } = require('../utils/apiUtils');
const { User } = require('../models/userModel');

router.get('/me', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const user = await User.findOne({ "_id": userId });
    res.status(200).json({
        "user": {
            "_id": user._id,
            "role": user.role,
            "email": user.email,
            "created_date": user.created_date
        }
    });
}));

router.delete('/me', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    await User.findOneAndRemove({ "_id": userId });
    res.status(200).json({ message: "Profile deleted successfully" });
}))

/*router.patch('/me/password', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const data = req.body;
    const { password } = req.user;
    console.log(password);
    await User.findByIdAndUpdate({ "_id": userId }, { $set: data },{new:true});
    res.status(200).json({message:"Password changed successfully"})
}))*/


module.exports = {
    userRouter:router
}