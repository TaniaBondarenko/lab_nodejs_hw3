const express = require('express');
const router = express.Router();
const { asyncWrapper } = require('../utils/apiUtils');
const { InvalidAuthentificationError, InvalidRequestError } = require('../utils/errors');
const { addLoadToShipper, getLoadsForDriver, getLoadsForShipper, getActiveLoad, getLoadById, updateLoadData, setNextState,findDriver} = require('../services/loadService');

router.post('/', asyncWrapper(async (req, res) => {
    const { userId, role } = req.user;
    if (role==='SHIPPER') {
        await addLoadToShipper(req.body, userId);
        res.status(200).json({ message: "load created successfully" });
    }
    if (role === "DRIVER") {
        throw new InvalidAuthentificationError('No authorized for drivers')
    }
}));

router.get('/', asyncWrapper(async (req, res) => {
    const { userId,role} = req.user;
    let status = req.query.status;
    if (status === undefined) {
        throw new InvalidRequestError('Not valid status');
    }
    if (role === "DRIVER") {
        if (status === "SHIPPED" || status === "ASSIGNED") {
            const loads = await getLoadsForDriver(userId, status);
            res.status(200).json({loads});
        } else {
            throw new InvalidRequestError('Not valid status');
        }
    } else if (role === "SHIPPER") {
        const loads = await getLoadsForShipper(userId);
        res.status(200).json({loads});
    }
}));

router.put('/:id', asyncWrapper(async (req, res) => {
     const { userId,role} = req.user;
    if (role === "SHIPPER") {
       
        const { id } = req.params;
        const data = req.body;
        
        console.log('data',data)
        await updateLoadData(id, userId, data);
    res.status(200).json({ "message": "Load details changed successfully" });}
}));

router.get('/active', asyncWrapper(async (req, res) => {
    const { userId,role } = req.user;
    if (role === "DRIVER") {
        const load = await getActiveLoad(userId)
        res.status(200).json({load}); 
    }
    else {
        throw new InvalidAuthentificationError('No authorized for shippers')
    }
}))

router.get('/:id', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;
    const load = await getLoadById(id, userId);
    if (!load) {
        throw new InvalidRequestError('No truck with such id found');
    }
    res.status(200).json({ load });
   
}));

router.patch('/active/state', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
        const newState = await setNextState(userId);
        res.status(200).json({
            message: `Load state changed to ${newState}`
        }) 
}));
//Post a user's load by id, search for drivers (abailable only for shipper role)   ---loadId
router.post('/active/:id/post', asyncWrapper(async (req, res) => {
    const { role } = req.user;
    const { id } = req.params;
    if (role === "SHIPPER") {
        await findDriver(id);
        res.status(200).json({
            "message": "Load posted successfully",
            "driver_found": true
        });
    } else {
        throw new InvalidAuthentificationError('No authorized for shippers')
    }
}));

module.exports = {
    loadRouter:router
}