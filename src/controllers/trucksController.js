const express = require('express');
const router = express.Router();
const { asyncWrapper } = require('../utils/apiUtils');
const { addTruckToDriver, getTrucksByDriverId,getTruckById,updateTruck,assignTruckToDriver } = require('../services/truckService');
const { InvalidAuthentificationError, InvalidRequestError } = require('../utils/errors');
const { Truck } = require('../models/truckModel');

router.post('/', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
        await addTruckToDriver(req.body, userId);
        res.status(200).json({ message: "Truck created successfully" });
}));

router.get('/', asyncWrapper(async (req, res) => {
    const { userId} = req.user;
    const trucks = await getTrucksByDriverId(userId);
    res.status(200).json( { trucks }  );
}));
//NEED TO CHECK FR CORRECT ERROR
router.get('/:id', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;
    const truck = await getTruckById(id, userId);
    if (!truck) {
        throw new InvalidRequestError('No truck with such id found');
    }
    res.status(200).json({ truck });
   
}));

router.put('/:id', asyncWrapper(async (req, res) => {
    const { userId} = req.user;
    const { id } = req.params;
    const { type } = req.body;
    await updateTruck(id, userId, type);
    res.status(200).json({ "message": "Truck details changed successfully" });
}));

router.post('/:id/assign', asyncWrapper(async (req, res) => {
    const { userId} = req.user;
    const { id} = req.params;
        await assignTruckToDriver(id, userId);
        res.status(200).json({ "message": "Truck assigned successfully" });
    
}));

router.delete('/:id', asyncWrapper(async (req, res) => {
    const { id } = req.params;
    await Truck.findOneAndRemove({ "_id": id });
    res.status(200).json({ message: "Truck deleted successfully" });
}))

module.exports = {
    truckRouter: router
}
