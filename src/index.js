const express = require('express');
const app = express();
const mongoose = require('mongoose');
const PORT = 8080;
const morgan = require('morgan');
const { userRouter } = require('./controllers/userControllers');
const { authRouter } = require('./controllers/authController');
const { truckRouter } = require('./controllers/trucksController');
const { loadRouter } = require('./controllers/loadController');

const { CommonHandlerError } = require('./utils/errors');
const { authMiddleware,checkPermissionMiddleware } = require('./middlewares/authMiddleware');

app.use(express.json());
app.use(morgan('tiny'));
app.use('/api/auth', authRouter);
app.use('/api/users', authMiddleware, userRouter);
app.use('/api/trucks', authMiddleware, checkPermissionMiddleware, truckRouter);
app.use('/api/loads',authMiddleware,loadRouter)


app.use((err, req, res, next) => {
    if (err instanceof CommonHandlerError) {
        return res.status(err.status).json({ message: err.message })
    }
    res.status(500).json({ message: err.message })
});

app.use((req, res, next) => {
    res.status(404).json({ message: 'Not found this' })
});

const start = async () => {
    try {
        await mongoose.connect('mongodb+srv://TaniaBond:m0ng0Pa$$@cluster0.dghps.mongodb.net/trackService',
            {
                useNewUrlParser: true, useUnifiedTopology: true
            });
        app.listen(PORT);
    } catch (err) {
        console.error(`Error on server start: ${err.message}`);
    }
    
}

start();
